from mesa import Agent



class MovingAgent(Agent):

    """
    Class implementing the agents moving arount the grid

    The citizen and Cop agents inherit its methods

    """
    grid = None
    x = None
    y = None
    pos = None
    def __init__(
        self,
        unique_id,
        model,
        vision,
    ):
        super().__init__(unique_id, model)
        self.vision = vision

    def random_move(self):
        """
        Move to any cell within Vision
        """
        if self.model.movement and self.empty_neighbours:
            new_pos = self.random.choice(self.empty_neighbours)
            self.model.grid.move_agent(self, new_pos)

    def swap_move(self, agent: Agent):
        victim_position = agent.pos
        if victim_position is not None:
            return
        agent.model.grid.remove_agent(agent)
        current_agent_position = self.pos
        self.model.grid.move_agent(self, victim_position)
        self.model.grid.position_agent(agent, current_agent_position[0], current_agent_position[1])




    def jail_move(self):
        if self.model.movement:
            self.model.grid.move_to_empty(self)


    def update_neighbourhood(self):
        self.neighbourhood = self.model.grid.get_lattice_neighbourhood(self.pos, self.model.moore, vision = self.vision)
        self.neighbours = self.model.grid.get_cell_list_contents(self.neighbourhood)
        self.empty_neighbours = [
            c for c in self.neighbourhood if self.model.grid.is_cell_empty(c)
        ]

from mesa import Model
from files.modified_single_grid import ModifiedSingleGrid
from mesa.time import RandomActivation
from .agent import PoliceMan, Citizen
from mesa.datacollection import DataCollector
from typing import Tuple


Coordinate = Tuple[int, int]

def count_active(model):
    active_count_var = 0
    for agent in model.schedule.agents:
        if agent.type == "Citizen":
            if agent.state == "Active" and agent.jail_sentence <= 0:
                active_count_var += 1

    return active_count_var


class EpsteinModel(Model):

    height= 40
    width = 40

    movement = True

    def __init__(
    self,
    height = 40,
    width = 40,
    movement = True,                    #variable
    max_iters = 1000,                   #variable
    citizen_vision = 4,                 #varibale
    citizen_density = 0.7,              #variable
    police_density = 0.074,             #variable
    police_vision = 1,                  #variable
    max_jail_sentence = 30,             #variable
    arrest_prob_constant = 2.3 ,
    active_threshold = 0.1,
    moore = True,
    model = True,
    ethnic_ratio = 0.5,
    reproductivity = 0.05,
    max_lifespan = 200,
    multi_legitimacy = False,
    legitimacy_1 = 0.7,                     #variable
    legitimacy_2 = 0.7,                     #variable
    legitimacy_3 = 0.7,                     #variable
    legitimacy_4 = 0.7,                     #variable
    move_towards_same_hardship = True,       #variable
    move_towards_similars = True,           #variable
    move_towards_similars_random = True,    #variable
    move_towards_rioters = True,
    remove_prisoners = True,                #variable
    ):
        super().__init__()
        self.height = height
        self.width = width
        self.moore = moore
        self.schedule = RandomActivation(self)
        self.grid = ModifiedSingleGrid(self.height, self.width, self.moore, torus= True,)
        self.iteration = 0
        self.movement = movement
        self.citizen_vision = citizen_vision
        self.citizen_density = citizen_density
        self.police_vision = police_vision
        self.police_density = police_density
        self.max_jail_sentence = max_jail_sentence
        self.arrest_prob_constant = arrest_prob_constant
        self.active_threshold  = active_threshold
        self.max_iters = max_iters
        self.model_type = model
        self.ethnic_ratio = ethnic_ratio
        self.reproductivity = reproductivity
        self.max_lifespan = max_lifespan
        self.unique_id = 0
        self.multi_legitimacy = multi_legitimacy            #variable
        self.legitimacy_1 = legitimacy_1                    #variable
        self.legitimacy_2 = legitimacy_2                    #variable
        self.legitimacy_3 = legitimacy_3                    #variable
        self.legitimacy_4 = legitimacy_4                    #variable
        self.move_towards_same_hardship = move_towards_same_hardship
        self.move_towards_similars = move_towards_similars  #variable
        self.move_towards_rioters = move_towards_rioters    #variable
        self.move_towards_similars_random = move_towards_similars_random
        self.remove_prisoners = remove_prisoners

        if self.police_density + self.citizen_density > 1:
            raise ValueError("Cop density + citizen density must be less than 1")

        if self.model_type:
            for (contents, x, y)in self.grid.coord_iter():
                if self.random.random() < self.police_density:
                    policeMan = PoliceMan(self.unique_id, self,  self.police_vision)
                    self.grid.position_agent(policeMan, x, y)
                    policeMan.pos = (x, y)
                    self.schedule.add(policeMan)
                    self.unique_id += 1
                elif self.random.random() < self.citizen_density:
                    citizen = Citizen(
                        self.unique_id,
                        self,
                        hardship=self.random.random(),
                        regime_legitimacy= self.get_legitimacy((x,y) ),
                        risk_aversion=self.random.random(),
                        threshold=self.active_threshold,
                        vision=self.citizen_vision,
                        )
                    self.datacollector = DataCollector(
                        model_reporters= {
                            "Active": count_active,
                        },
                    )
                    citizen.pos= (x,y)
                    self.grid.position_agent(citizen, x, y)
                    self.schedule.add(citizen)
                    self.unique_id +=1
        else:
            for (contents, x, y)in self.grid.coord_iter():
                if self.random.random() < self.police_density:
                    policeMan = PoliceMan(self.unique_id, self,  self.police_vision)
                    self.grid.position_agent(policeMan, x, y)
                    policeMan.pos = (x, y)
                    self.schedule.add(policeMan)
                    self.unique_id += 1
                elif self.random.random() < self.citizen_density:
                    creed = "ethnicity_1"
                    if self.random.random() < self.ethnic_ratio:
                        creed = "ethnicity_2"

                    citizen = Citizen(
                        self.unique_id,
                        self,
                        hardship=self.random.random(),
                        regime_legitimacy=self.get_legitimacy(self, (x,y) ),
                        risk_aversion=self.random.random(),
                        threshold=self.active_threshold,
                        vision=self.citizen_vision,
                        ethnicity = creed,
                        max_lifespan = self.max_lifespan,
                        reproductivity = self.reproductivity,
                        )
                    citizen.pos= (x,y)
                    self.grid.position_agent(citizen, x, y)
                    self.schedule.add(citizen)
                    self.unique_id +=1
                    self.datacollector = DataCollector(
                        model_reporters= {
                            "Active": count_active,
                        },
                    )
        self.running = True

    def step(self):
        self.datacollector.collect(self)
        self.schedule.step()
        self.iteration +=1
        if self.iteration > self.max_iters:
            self.running = False

    def get_legitimacy(self, pos: Coordinate):
        x,y = pos
        if not self.multi_legitimacy:
            return self.legitimacy_1
        else:
            if(x <20 and y < 20):
                return self.legitimacy_1
            elif(20 <= x and y < 20):
                return self.legitimacy_2
            elif(x < 20 and y >= 20):
                return self.legitimacy_3
            else:
                return self.legitimacy_4

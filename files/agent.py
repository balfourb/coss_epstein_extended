import math

from mesa import Agent
from files.moving_agent import MovingAgent


class Citizen(MovingAgent):

    def __init__(
        self,
        unique_id,
        model,
        vision,
        hardship,
        regime_legitimacy,
        risk_aversion,
        threshold,
        ethnicity = "None",
        reproductivity = 0.05,
        max_lifespan = 200,
    ):
        super().__init__(unique_id, model, vision)
        self.type = "Citizen"
        self.hardship = hardship
        self.regime_legitimacy = regime_legitimacy
        self.risk_aversion = risk_aversion
        self.threshold = threshold
        self.state = "Quiescent"
        self.vision = vision
        self.jail_sentence = 0
        self.grievance = hardship * (1-regime_legitimacy)
        self.arrest_probability = None
        self.ethnicity = ethnicity
        self.reproductivity = reproductivity
        self.lifespan = self.random.randint(0, max_lifespan)
        self.max_neighbour_list = None
        self.min_neighbour_list = None
        self.max_neighbour = None
        self.min_neighbour = None
        self.min_differences = None
        self.max_differences = None


    def step(self):
        if not self.model.model_type:
            if (self.lifespan <= 0):
                self.model.grid._remove_agent(self.pos,self)
                self.pos = None
                self.model.schedule.remove(self)
                return

        if self.model.remove_prisoners and self.jail_sentence:
            self.jail_sentence -= 1
            if self.jail_sentence == 0:
                self.model.grid.move_to_empty(self)
            return
        elif not self.model.remove_prisoners and self.jail_sentence:
            self.jail_sentence -= 1
            self.jail_move()
            return



        if self.model.model_type:
            if not self.model.move_towards_similars:
                self.update_neighbourhood()
                self.update_estimated_arrest_probability()

                net_risk = self.risk_aversion * self.arrest_probability

                if self.state == "Quiescent" and (self.grievance - net_risk) > self.threshold:
                    self.state = "Active"
                elif self.state == "Active" and (self.grievance - net_risk) <= self.threshold:
                    self.state = "Quiescent"

                if self.model.movement:
                    self.random_move()
            else:
                if self.model.move_towards_similars_random:
                    self.update_citizen_neighbourhood_type_one()
                    self.update_estimated_arrest_probability()
                    net_risk = self.risk_aversion * self.arrest_probability

                    if self.state == "Quiescent" and (self.grievance - net_risk) > self.threshold:
                        self.state = "Active"
                    elif self.state == "Active" and (self.grievance - net_risk) <= self.threshold:
                        self.state = "Quiescent"

                    if self.model.movement:
                        self.move_towards_similar()
                else:
                    self.update_citizen_neighbourhood_type_two()
                    self.update_estimated_arrest_probability()
                    net_risk = self.risk_aversion * self.arrest_probability

                    if self.state == "Quiescent" and (self.grievance - net_risk) > self.threshold:
                        self.state = "Active"
                    elif self.state == "Active" and (self.grievance - net_risk) <= self.threshold:
                        self.state = "Quiescent"

                    if self.model.movement:
                        self.move_towards_similar()

        else:
            self.update_neighbourhood()
            self.update_estimated_arrest_probability()
            self.duplicate()
            self.update_neighbourhood()

            net_risk = self.risk_aversion * self.arrest_probability

            if(self.grievance - net_risk) > self.threshold:
                self.state = "Active"
                self.kill_someone()
                self.update_neighbourhood()
            else:
                self.state = "Quiescent"

            if self.model.movement:
                self.random_move()

    def update_estimated_arrest_probability(self):

        cops_in_vision = len([c for c in self.neighbours if c.type == "Police"])
        actives_in_vision = 1.0  # citizen counts herself
        for c in self.neighbours:
            if (
                c.type == "Citizen"
                and c.state == "Active"
                and c.jail_sentence == 0
            ):
                actives_in_vision += 1
        self.arrest_probability = 1 - math.exp(
            -1 * self.model.arrest_prob_constant * (cops_in_vision / actives_in_vision)
        )

    def duplicate (self):
        if(self.empty_neighbours and (self.random.random()< self.reproductivity)):
            new_agent_pos = self.random.choice(self.empty_neighbours)
            clone = Citizen(
                self.model.unique_id,
                self.model,
                hardship = self.hardship,
                regime_legitimacy = self.regime_legitimacy,
                risk_aversion = self.risk_aversion,
                threshold = self.threshold,
                vision = self.vision,
                ethnicity = self.ethnicity,
                reproductivity = self.reproductivity,
                max_lifespan = self.random.randint(0,self.model.max_lifespan),
            )
            self.model.unique_id += 1
            clone.pos = new_agent_pos
            self.model.grid.position_agent(clone, new_agent_pos[0], new_agent_pos[1])
            self.model.schedule.add(clone)

    def kill_someone(self):
        other_tribe_members = []
        for agent in self.model.schedule.agents:
            if(agent.type == "Citizen"):
                if(agent.ethnicity != self.ethnicity):
                    other_tribe_members.append(agent)
        if(len(other_tribe_members) != 0):
            victim = self.random.choice(other_tribe_members)
            self.model.grid.remove_agent(victim)
            self.model.schedule.remove(victim)

    def update_citizen_neighbourhood_type_one(self):

        self.update_neighbourhood()
        self.max_neighbour = None
        self.min_neighbour = None
        self.max_differences = []
        self.min_differences = []
        self.max_neighbour_list = []
        self.min_neighbour_list = []

        for n in self.neighbours:
            if(n.type == "Police"):
                continue
            
            difference = 0 #not sure if python needs this for the scope...
            if self.model.move_towards_same_hardship:
                difference = abs(self.hardship - n.hardship)
            else:
                difference = abs(self.grievance - n.grievance)

            sorted_max_differences = sorted(self.max_differences)
            sorted_min_differences = sorted(self.min_differences)



            if len(self.max_neighbour_list) <= 3:
                self.max_neighbour_list.append(n)
                self.max_differences.append(difference)
            elif sorted_max_differences[2] < difference:
                sorted_max_differences[2] = difference

                sorted_neighbours = []
                if self.model.move_towards_same_hardship:
                    sorted_neighbours = sorted(self.max_neighbour_list, key = lambda Citizen: abs(Citizen.hardship - self.hardship))
                else:
                    sorted_neighbours = sorted(self.max_neighbour_list, key = lambda Citizen: abs(Citizen.grievance - self.grievance))

                sorted_neighbours[2] = n
                self.max_neighbour = self.random.choice(self.max_neighbour_list)

            if len(self.min_neighbour_list) <= 3:
                self.min_neighbour_list.append(n)
                self.min_differences.append(difference)
            elif sorted_min_differences[2] > difference:
                sorted_min_differences[2] = difference

                sorted_neighbours = []
                if self.model.move_towards_same_hardship:
                    sorted_neighbours = sorted(self.max_neighbour_list, key = lambda Citizen: abs(Citizen.hardship - self.hardship))
                else:
                    sorted_neighbours = sorted(self.max_neighbour_list, key = lambda Citizen: abs(Citizen.grievance - self.grievance))

                sorted_neighbours[2] = n
                self.max_neighbour = self.random.choice(self.min_neighbour_list)

    def update_citizen_neighbourhood_type_two(self):
        self.update_neighbourhood()
        self.max_neighbour = None
        self.min_neighbour = None
        self.max_difference = 0
        self.min_difference = 0

        for n in self.neighbours:
            if(n.type == "Police"):
                continue
            difference = abs(self.hardship - n.hardship)
            old_difference = self.min_difference
            old_max_difference = self.max_difference

            if self.max_neighbour == None:
                self.max_neighbour = n
                self.max_difference = difference
            elif self.max_difference < difference:
                self.max_difference = difference
                self.max_neighbour = n

            if self.min_neighbour == None:
                self.min_neighbour = n
                self.min_difference = difference
            elif self.min_difference > difference:
                self.min_difference = difference
                self.min_neighbour = n


    def move_towards_similar(self):
        if self.min_neighbour is None:
            self.random_move()
            return
        if self.model.move_towards_similars_random:
            self.min_neighbour.update_citizen_neighbourhood_type_one()
        else:
            self.min_neighbour.update_citizen_neighbourhood_type_two()
        if self.min_neighbour.max_neighbour is None:
            self.random_move()
            return
        victim = self.min_neighbour.max_neighbour
        self.swap_move(victim)



class PoliceMan(MovingAgent):

    def __init__( self, unique_id, model, vision):
        super().__init__(unique_id, model, vision)
        self.type = "Police"
        self.model = model
        self.min_rioter = None
        self.min_grievance = 0

    def step(self):

        if not self.model.move_towards_rioters:
            self.update_neighbourhood()
            self.arrest_random()
            if self.model.movement:
                self.random_move()
        else:
            self.update_police_neighbourhood()
            self.arrest_random()
            if self.model.movement:
                if self.min_rioter is None:
                    self.random_move()
                else:
                    self.swap_move(self.min_rioter)
            return


    def update_police_neighbourhood(self):
        self.update_neighbourhood()
        for n in self.neighbours:
            if n.type == "Police":
                continue
            if n.grievance < self.min_grievance:
                self.min_rioter = n

    def arrest_random(self):
        active_neighbours = []
        for agent in self.neighbours:
            if(
            agent.type == "Citizen"
            and agent.state == "Active"
            and agent.jail_sentence == 0
            ):
                active_neighbours.append(agent)
        if active_neighbours:
            arrestee = self.random.choice(active_neighbours)
            sentence = self.random.randint(0, self.model.max_jail_sentence)
            arrestee.jail_sentence = sentence
            if self.model.remove_prisoners:
                self.model.grid._remove_agent(arrestee.pos,arrestee)

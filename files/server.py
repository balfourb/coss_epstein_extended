from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.modules import CanvasGrid, ChartModule
from mesa.visualization.UserParam import UserSettableParameter

from files.agent import Citizen, PoliceMan
from files.model import EpsteinModel


POLICE_COLOR = "#000000"               #black
AGENT_QUIET_COLOR = "#0066CC"       #blue
AGENT_REBEL_COLOR = "#CC0000"       #red
JAIL_COLOR = "#757575"              #grey

AGENT_ETHNIC_1_QUIET_COLOR = "#008000" #Green
AGENT_REBEL_COLOR_1 = "#00FF44"        #Light Green

AGENT_ETHNIC_2_QUIET_COLOR = "#0066CC" #blue
AGENT_REBEL_COLOR_2 = "#00FFFB"        #lIGHT Blue

def citizen_police_portrayal(agent):
    if agent is None:
        return

    portrayal = {
        "Shape": "circle",
        "x": agent.pos[0],
        "y": agent.pos[1],
        "Filled": "true",
        "Layer": 0,
    }
    if agent.model.model_type:
        if agent.type == "Citizen":
            portrayal["r"] = 0.8
            if agent.jail_sentence == 0:
                if agent.state == "Quiescent":
                    portrayal["Color"] = AGENT_QUIET_COLOR
                else:
                    portrayal["Color"] = AGENT_REBEL_COLOR
            else:
                portrayal["Color"] = JAIL_COLOR
        elif agent.type == "Police":
            portrayal["r"] = 0.5
            portrayal["Color"] = POLICE_COLOR
    else:
        if agent.type == "Citizen":
            portrayal["r"] = 0.8
            if agent.jail_sentence == 0:
                if agent.state == "Quiescent":
                    if agent.ethnicity == "ethnicity_1":
                        portrayal["Color"] = AGENT_ETHNIC_1_QUIET_COLOR
                    else:
                        portrayal["Color"] = AGENT_ETHNIC_2_QUIET_COLOR
                else:
                    if agent.ethnicity == "ethnicity_1":
                        portrayal["Color"] = AGENT_REBEL_COLOR_1
                    else:
                        portrayal["Color"] = AGENT_REBEL_COLOR_2
            else:
                portrayal["Color"] = JAIL_COLOR
        elif isinstance(agent, PoliceMan):
            portrayal["r"] = 0.5
            portrayal["Color"] = POLICE_COLOR
    return  portrayal

def grievance_rendering(agent):
    if agent is None:
        return
    portrayal = {
        "Shape": "rect",
        "x": agent.pos[0],
        "y": agent.pos[1],
        "Filled": "true",
        "w": 1,
        "h": 1,
        "Layer":0,
    }
    if agent.type == "Citizen":
        portrayal["Color"] = get_hex_colour(agent)
    elif agent.type == "Police":
        portrayal["Color"] = POLICE_COLOR
    return portrayal


def get_hex_colour(agent):
    red = 255
    green = 255 - int(255 * agent.grievance)
    blue = green
    return "#%02x%02x%02x" % (red, green, blue)



canvas_element = CanvasGrid(citizen_police_portrayal, 40, 40 , 480, 480)
grievance_render = CanvasGrid(grievance_rendering, 40, 40 , 480, 480)
line_chart = ChartModule(
        [
            {"Label": "Active", "Color": AGENT_REBEL_COLOR}
        ]
)

model_params = {
    "model":UserSettableParameter("checkbox", "Model: ON=1, OFF=2", True),
    "movement": UserSettableParameter("checkbox", "Do the Agents Move?", True),
    "moore": UserSettableParameter("checkbox", "Use Moore or von Neumann neighbour method", True),
    "multi_legitimacy": UserSettableParameter("checkbox", "The grid is separated to have different legitimacies", False),
    "move_towards_same_hardship": UserSettableParameter("checkbox", "Move towards ON=hardship, OFF=Grievance", True), #makes sense in the different legitimacies setting
    "move_towards_similars": UserSettableParameter("checkbox", "Do the Citizend move towards other citizens with similar", True),
    "move_towards_similars_random": UserSettableParameter("checkbox", "Do the Citizend move randomly towards other citizens with similar", True),
    "move_towards_rioters":UserSettableParameter("checkbox", "Do the police move towards citizens who are rioting", True),
    "remove_prisoners":UserSettableParameter("checkbox", "remove the prisoners from the grid", True),
    "legitimacy_1": UserSettableParameter ("slider", "Legitimacy: Global or bottom left", 0.72, 0, 1, 0.01),
    "legitimacy_2": UserSettableParameter ("slider", "Legitimacy: bottom right", 0.72, 0, 1, 0.01),
    "legitimacy_3": UserSettableParameter ("slider", "Legitimacy: top left", 0.72, 0, 1, 0.01),
    "legitimacy_4": UserSettableParameter ("slider", "Legitimacy: top right", 0.72, 0, 1, 0.01),
    "citizen_density":UserSettableParameter("slider", "Citizen Density", 0.7, 0, 1, 0.01),
    "police_density":UserSettableParameter("slider", "PoliceMan Density", 0.04, 0 , 0.1, 0.001),
    "citizen_vision": UserSettableParameter("slider", "Citizen Vision", 7, 0, 24, 1),
    "police_vision": UserSettableParameter("slider", "PoliceMan Vision", 7, 0, 24, 1),
    "max_jail_sentence": UserSettableParameter ("number", "Maximum Jail Sentence", 30),
    "max_iters":UserSettableParameter("number", "maximum iterations",1000),
}

server = ModularServer(
    EpsteinModel, [canvas_element,grievance_render, line_chart], "Epstein Civil Violence", model_params
)

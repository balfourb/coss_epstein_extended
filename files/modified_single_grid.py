import itertools

import numpy as np

from typing import Any, Dict, Iterable, Iterator, List, Optional, Set, Tuple, Union
from mesa.agent import Agent
from mesa.space import SingleGrid

Coordinate = Tuple[int, int]
GridContent = Union[Optional[Agent], Set[Agent]]

class ModifiedSingleGrid(SingleGrid):
    def __init__(self, width: int, height: int, moore:bool, torus: bool) -> None:
        super().__init__(width, height, torus)
        self.moore = moore

    def modified_iter_neighborhood(
        self,
        pos: Coordinate,
        moore: bool,
        include_center: bool = False,
        vision: int = 7,
    ) -> Iterator[Coordinate]:

        radius = self.caluculate_radius(vision)
        counter =  0
        x, y = pos
        coordinates = set()  # type: Set[Coordinate]
        for dy in range(-radius, radius + 1):
            for dx in range(-radius, radius + 1):
                if dx == 0 and dy == 0 and not include_center:
                    continue
                # Skip coordinates that are outside manhattan distance
                if not moore and abs(dx) + abs(dy) > radius:
                    continue
                # Skip if not a torus and new coords out of bounds.
                if not self.torus and (
                    not (0 <= dx + x < self.width) or not (0 <= dy + y < self.height)
                ):
                    continue

                px, py = self.torus_adj((x + dx, y + dy))

                # Skip if new coords out of bounds.
                if self.out_of_bounds((px, py)):
                    continue
                coords = (px, py)
                if coords not in coordinates and counter <= vision:
                    counter +=1
                    coordinates.add(coords)
                    yield coords


    def get_lattice_neighbourhood(
        self,
        pos: Coordinate,
        moore: bool,
        include_center: bool = False,
        vision: int = 7,
    ):
        return list(self.modified_iter_neighborhood(pos, moore, include_center, vision))

    def caluculate_radius(self,vision: int = 7):
        if self.moore:
            if(vision <= 8):
                return 1
            elif(vision <= 24):
                return 2
            else:
                return 3
        else:
            if (vision <= 4):
                return 1
            elif(vision <= 8):
                return 2
            elif (vision <= 12):
                return 3
            elif (vision <= 16):
                return 4
            elif (vision <= 20):
                return 5
            else:
                return 6
